$(function () {
    $( "#from" ).datepicker({
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    $( "#getData").on('click', function() {
      if ($('#from').val() && $('#to').val()) {
        getConsumption($('#from').val(), $('#to').val());
      }
    });
    $( "#editCost").on('click', '#save', function() {
      updateRate({
        aveRate: $('#cost').val()
      });
    });
    getConsumption();
    setInterval(function(){
      getConsumption();
      console.log('data collected');
    }, 5000);

});

function getConsumption(from, to) {
  var today = new Date();
  var from = from
    ? new Date(from)
    : new Date(today);
  from.setHours(0);
  from.setMinutes(0);
  from.setSeconds(0);
  var to = to
    ? new Date(to)
    : new Date(today);
  to.setHours(23);
  to.setMinutes(59);
  to.setSeconds(59);
  $.ajax({
    url: '/v1/electricMeters?from='+ from +'&to=' + to,
    type: 'GET',
    success: function(data) {
      plotData(data);
    },
    error: function(e) {
      console.log(e.responseText);
    }
  });
}

function updateRate(data) {
  $.ajax({
    url: '/v1/users/aveRate',
    type: 'PUT',
    data: JSON.stringify(data),
    contentType: 'application/json',
    dataType: 'json',
    success: function(data) {
      displayRate(data);
      $('#editCost').modal('hide');
    },
    error: function(e) {
      console.log(e.responseText);
    }
  });
}

function plotData(data) {
  $('#chart').highcharts({
    title: {
      text: 'Total Electric Energy Consumption',
      x: -15 //center
    },
    xAxis: {
      categories: data.xAxis
    },
    yAxis: {
      title: {
        text: 'Electric Energy Consumption (kWh)'
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: '#808080'
      }]
    },
    tooltip: {
      valueSuffix: 'kWh'
    },
    /*legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      borderWidth: 0
    },*/
    series: [{
      data: data.yAxis,
      name: 'Hourly Consumption'
    }]
  });
  $('#totalCost').text('Php ' + data.totalCost);
  $('#carbonFootprint').text(data.carbonFootprint + ' kg');
  $('#totalConsumption').text(data.totalConsumption + ' kWh');
}

function getLabel(group) {
  switch(group) {
    case 'hour':
      return 'Hourly Consumption';
    case 'day':
      return 'Daily Consumption';
    case 'month':
      return 'Monthly Consumption';
  }
}

function displayRate(data) {
  $('#cost').val(data.aveRate);
  getConsumption($('#from').val(), $('#to').val());
}