$(function() {
  $.ajaxSetup({ cache: false });
  $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
    FB.init({
      appId: '412951565496029',
      version: 'v2.4'
    });
  });
  $('.inner.cover .cover-heading p.lead').on('click', 'a#login', function(e) {
    FB.getLoginStatus(checkLoginStatus);
  });
  $('a#featuresLink').bind('click',function (e) {
      e.preventDefault();
      scrollToElement('#features', 600);
  });
  $('a#howItWorksLink').bind('click',function (e) {
    e.preventDefault();
    scrollToElement('#howItWorks', 600);
  });
});


function getLoginStatus() {
  FB.getLoginStatus(checkLoginStatus);
}
function checkLoginStatus(res) {
  if (res.status === 'connected') {
    login(res.authResponse.userID, res.authResponse.accessToken);
  } else {
    FB.login(function(res) {
      if (res.authResponse) {
        login(res.authResponse.userID, res.authResponse.accessToken);
      }
    });
  }
}

function login(fbId, accessToken) {
   $.ajax({
    url: '/v1/sessions',
    type: 'POST',
    contentType: 'application/json',
    dataType: 'json',
    headers: {
      fbId: fbId,
      accessToken: accessToken
    },
    success: function(data) {
      window.location.href = '/dashboard';
    },
    error: function(e) {
      console.log(e.responseText);
    }
  });
}

var scrollToElement = function(el, ms){
    var speed = (ms) ? ms : 600;
    $('html,body').animate({
        scrollTop: $(el).offset().top
    }, speed);
}