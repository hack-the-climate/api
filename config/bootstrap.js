/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  if(sails.config.populate.activate) {
    async.eachSeries(sails.config.populate.items, function(item, cb) {
      Populate.add(item, sails.config.populate[item], function(err) {
        cb(err);
      });
    }, function(err){
      if (err) {
        sails.log.error('Error in data initialization');
      }
      cb();
    });
  } else {
    cb();
  }
};
