module.exports.populate = {
  activate: true,
  items: [ 'user', 'electricmeter'],
  user: [
    {
      name: 'Erica Padilla',
      fbId: '10206527544820112',
      id: '55eab871b7e70a161994cf74'
    }
  ],
  electricmeter: [
    {
      value: 4855,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T00:00:00Z')
    },
    {
      value: 4865,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T01:00:00Z')
    },
    {
      value: 4873,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T02:00:00Z')
    },
    {
      value: 4890,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T03:00:00Z')
    },
    {
      value: 4900,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T04:00:00Z')
    },
    {
      value: 4910,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T05:00:00Z')
    },
    {
      value: 4920,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T06:00:00Z')
    },
    {
      value: 4930,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T07:00:00Z')
    },
    {
      value: 4950,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T08:00:00Z')
    },
    {
      value: 4955,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T09:00:00Z')
    },
    {
      value: 4960,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T10:00:00Z')
    },
    {
      value: 4965,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T11:00:00Z')
    },
    {
      value: 4970,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T12:00:00Z')
    },
    {
      value: 4980,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T13:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T14:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T15:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T16:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T17:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T18:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T19:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T20:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T21:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T22:00:00Z')
    },
    {
      value: 4990,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-04T23:00:00Z')
    },
    {
      value: 5000,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T00:00:00Z')
    },
    {
      value: 5010,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T01:00:00Z')
    },
    {
      value: 5019,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T02:00:00Z')
    },
    {
      value: 5027,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T03:00:00Z')
    },
    {
      value: 5027,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T04:00:00Z')
    },
    {
      value: 5027,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T05:00:00Z')
    },
    {
      value: 5040,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T06:00:00Z')
    },
    {
      value: 5047,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T07:00:00Z')
    },
    {
      value: 5063,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T08:00:00Z')
    },
    {
      value: 5078,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T09:00:00Z')
    },
    {
      value: 5080,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T10:00:00Z')
    },
    {
      value: 5095,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T11:00:00Z')
    },
    {
      value: 5100,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T12:00:00Z')
    },
    {
      value: 5105,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T13:00:00Z')
    },
    {
      value: 5109,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T14:00:00Z')
    },
    {
      value: 5113,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T15:00:00Z')
    },
    {
      value: 5117,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T16:00:00Z')
    },
    {
      value: 5120,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T17:00:00Z')
    },
    {
      value: 5122,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T18:00:00Z')
    },
    {
      value: 5123,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T19:00:00Z')
    },
    {
      value: 5124,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T20:00:00Z')
    },
    {
      value: 5125,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T21:00:00Z')
    },
    {
      value: 5127,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T22:00:00Z')
    },
    {
      value: 5128,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-05T23:00:00Z')
    },
    {
      value: 5129,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T00:00:00Z')
    },
    {
      value: 5132,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T01:00:00Z')
    },
    {
      value: 5134,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T02:00:00Z')
    },
    {
      value: 5146,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T03:00:00Z')
    },
    {
      value: 5156,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T04:00:00Z')
    },
    {
      value: 5162,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T05:00:00Z')
    },
    {
      value: 5166,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T06:00:00Z')
    },
    {
      value: 5170,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T07:00:00Z')
    },
    {
      value: 5172,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T08:00:00Z')
    },
    {
      value: 5175,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T09:00:00Z')
    },
    {
      value: 5178,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T10:00:00Z')
    },
    {
      value: 5181,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T11:00:00Z')
    },
    {
      value: 5184,
      user: '55eab871b7e70a161994cf74',
      createdAt: new Date('2015-09-06T12:00:00Z')
    },
  ]
};
