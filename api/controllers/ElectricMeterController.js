module.exports = {
  create: function(req, res) {
    var params = Validation.containKeys(require('rekuire')('electricMeter'), req.body);

    if (!params.valid) {
      return res.error(params.error);
    }
    
    ElectricMeter.create({
      value: req.body.value,
      user: req.session.user.id
    }, function(err, entry) {
      if (err) {
        return res.error(Errors.raise('DB_ERROR'));
      }
      res.json(201, entry)
    });
  },
  list: function(req, res) {
    /*var params = Validation.containKeys(require('rekuire')('electricMeterList'), req.query);

    if (!params.valid) {
      return res.error(params.error);
    }*/

    var criteria = {
      where: {
        user: req.session.user.id,
        createdAt: {
          '>=': req.query.from,
          '<=': req.query.to
        }
      },
      sort: { createdAt: 1 }
    };

    ElectricMeter.find(criteria, function(err, list) {
      if (err) {
        return res.error(Errors.raise('DB_ERROR'));
      }

      var data = Utility.groupData(list, req.query.groupBy);
      console.dir(req.user);
      data.totalCost = req.session.user.aveRate * data.totalConsumption;
      return res.json(200, data);

    });
  }
}