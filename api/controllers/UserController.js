var TAG = '[UserController]';

module.exports = {
  update: function(req, res) {
    var ACTION = '[update]';
    User.update(req.session.user.id, req.body, function(err, user) {
      if (err) {
        sails.log.error(TAG, ACTION, err);
        return res.error(Errors.raise('DB_ERROR'));
      }
      if (user.length === 0) {
        return res.error(Errors.raise('NOT_FOUND'))
      }
      req.session.user = user[0];
      res.json(200, user[0]);
    });
  }
}