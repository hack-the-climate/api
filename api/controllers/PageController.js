module.exports = {
  dashboard: function(req, res) {
    if (!req.session.user) {
      return res.redirect('/');
    }
    res.locals.layout = 'dashboard-layout';
    res.view('dashboard', req.session.user);
  }
}