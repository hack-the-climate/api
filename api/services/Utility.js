module.exports = {
  groupData: function(data, group) {
    var ret = { xAxis: [], yAxis: [], totalConsumption: 0, carbonFootprint: 0 };

    var list = _.groupBy(Utility.computeConsumption(data), function(item) {
      return Utility.getGrouping(item, group);
    });

    for(var group in list){
      list[group] = Utility.getTotalConsumption(list[group]);
    }
    
    for(var property in list){
      ret.xAxis.push(property);
      ret.yAxis.push(list[property]);
    }

    for(var i in ret.yAxis) { ret.totalConsumption += ret.yAxis[i]; }

    ret.carbonFootprint = ret.totalConsumption * 0.548;
    return ret;
  },
  getGrouping: function(data, group) {
    var n = data.createdAt.toISOString().length;
    switch (group) {
      case 'month' :
        n = 7; break;
      case 'hour' :
        n = 13;
      case 'day' :
        n = 10;
    }
    return data.createdAt.toISOString().substring(0,n+1);
  },
  computeConsumption: function(data) {
    for (var i = 0; i<data.length; i++) {
      data[i].consumption = (i === 0)
        ? 0
        : data[i].value - data[i-1].value;
    }
    return data;
  },
  getTotalConsumption: function(data) {
    var total = 0;
    data.forEach(function(item) {
      total += parseFloat(item.consumption);
    });
    return total;
  },
  hasKeys: function(obj) {
    if (obj && typeof obj === 'object') {
      return Object.keys(obj).length > 0;
    }
    return false;
  },

  logRequest: function(req) {
    if (typeof  req === 'undefined' ) {
      throw new Error('Please pass the request info.');
    }

    var request = "Request :: " + req.method + req.url;
    /*if (Utility.hasKeys(req.headers)) {
      request += '\nHeaders: ' + JSON.stringify(req.headers);
    }*/
    if (Utility.hasKeys(req.params)) {
      request += '\nParams: ' + JSON.stringify(req.params);
    }
    if (Utility.hasKeys(req.query)) {
      request += '\nQuery: ' + JSON.stringify(req.query);
    }
    if (Utility.hasKeys(req.body)) {
      request += '\nBody: ' + JSON.stringify(req.body);
    }
    sails.log.info(request)
  },


}