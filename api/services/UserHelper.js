var TAG = '[UserHelper]';

module.exports = {
  find: function(criteria, cb) {
    var ACTION = '[findUser]';
    User.findOne(criteria, function(err, entry) {
      if (err) {
        sails.log.error(TAG, ACTION, err);
        return cb(Errors.raise('DB_ERROR'));
      }
      cb(null, entry);
    });
  },
  create: function(criteria, cb) {
    var ACTION = '[create]';
    User.create(criteria, function(err, entry) {
      if (err) {
        sails.log.error(TAG, ACTION, err);
        return cb(Errors.raise('DB_ERROR'));
      }
      cb(null, entry);
    });
  },
  findOrCreate: function(fbId, token, cb) {
    async.auto({
      find: function(cb) {
        UserHelper.find({
          fbId: fbId
        }, cb);
      },
      requestFBDetails: [ 'find', function(cb, result) {
        if (result.find) {
          return cb();
        }
        FBClient.getUserDetails(token, cb);
      }],
      create: [ 'requestFBDetails', function(cb, result) {
        if (result.find) {
          return cb();
        }
        var user = result.requestFBDetails;
        UserHelper.create({
          name: user.name,
          fbId: user.id
        }, cb)
      }]
    }, function(err, result) {
      console.log(result);
      cb(err, result.find || result.create);
    });
  }
}