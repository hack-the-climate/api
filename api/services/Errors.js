module.exports = {
  get: function (tag) {
    var errors = {
      MISSING_INVALID_PARAMS: { status: 400, error: { code: -1, message: 'Missing/invalid parameters', 'parameters': [] }},
      UNAUTHORIZED: { status: 401, error: { code: -2, message: 'Unauthorized' }},
      NOT_FOUND: { status: 404, error: { code: -3, message: 'Not found' }},
      INTERNAL_SERVER_ERROR: { status: 500, error: { code: -4, message: 'Internal server error' }},
      DB_ERROR: { status: 503, error: { code: -5, message: 'Database error/unavailable' }},
      FB_SERVICE_ERROR: { status: 503, error: { code: -6, message: 'FB service error/unavailable' }}
    };
    return errors[tag] || errors['INTERNAL_SERVER_ERROR'];
  },
  raise: function (e) {
    var error = JSON.parse(JSON.stringify(this.get(e)));
    return error;
  },
  getParam: function (tag) {
    var params = {
      value: {
        field: 'value',
        description: 'Must be a number'
      },
      from: {
        field: 'from',
        description: 'Must be an ISO-string formatted date'
      },
      to: {
        field: 'to',
        description: 'Must be an ISO-string formatted date'
      },
      groupBy: {
        field: 'groupBy',
        description: 'Optional. Must be any of the ff: hour, day, or month.'
      }
    };
    return params[tag] || null;
  },
};
