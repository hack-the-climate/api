var TAG = '[FBClient]';
var request = require('request');

module.exports = {
  getUserDetails: function(token, cb) {
    var ACTION = '[getUserDetails]';
    var url = 'https://graph.facebook.com/me?access_token=' + token;
    request.get(url, function(err, res, body) {
      if (err) {
        sails.log.error(TAG, ACTION, err);
        return cb(Errors.raise('FB_SERVICE_ERROR'));
      }
      sails.log.info(TAG, ACTION, body);
      if (res.statusCode === 200) {
        cb(null, JSON.parse(body));
      } else {
        cb(Errors.raise('FB_SERVICE_ERROR'));
      }
    });
  }
};