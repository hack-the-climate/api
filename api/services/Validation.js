var validator = require('validator');

module.exports = {
  containKeys: function(params, body, file){
    var _this = this;
    if (typeof params === 'undefined' || params.constructor !== Array) {
      throw new Error('Please pass an array of valid params obj.');
    }
    if (typeof body === 'undefined') {
      throw new Error('Please pass an object.');
    }
    var errors = _this.hasValidationErrors(params, body, file);
    if (errors.length == 0) {
      return { valid: true, error: null };
    }
    var ret = { valid: false, error: Errors.raise('MISSING_INVALID_PARAMS') };
    ret.error.error.parameters = errors;
    return ret;
  },
  hasValidationErrors: function(params, body, file){
    var _this = this;
    var errors = [];
    if (typeof params === 'undefined' || params.constructor !== Array) {
      throw new Error('Please pass an array of valid params obj.');
    }

    params.forEach(function(param) {
      var obj = (param.type == 'file') ? file : body;
      var opts = {
        minLength: param.minLength,
        maxLength: param.maxLength,
        in: param.in
      }
      if ( ( param.required && ( !obj || !obj.hasOwnProperty(param.name) || !_this.isValidFormat(param.type, obj[param.name], opts) )) ||
        ( !param.required && obj && obj.hasOwnProperty(param.name) && !_this.isValidFormat(param.type, obj[param.name], opts) ) ){
        if (param.json && obj[param.name]) {
          try {
            var jsonErrors = [];
            if (param.type == 'jsonArray') {
              jsonErrors = _this.hasArrayValidationErrors(param.name, require('rekuire')(param.json), obj[param.name]);
            } else if (param.type == 'json'){
              jsonErrors = _this.alterErrorFields(param.name, _this.hasValidationErrors(require('rekuire')(param.json), obj[param.name]));
            }
            errors = errors.concat(jsonErrors);
          } catch(e) {
            sails.log.error(e);
            errors.push(Errors.getParam(param.name));
          }
        } else {
          if (! (!param.json && param.type == 'json' && typeof (obj[param.name]) == 'object' ) ){
            errors.push(Errors.getParam(param.name));
          }
        }
      }
    });

    return errors;
  },
  hasArrayValidationErrors: function(name, params, value) {
    if (typeof name === 'undefined') {
      throw new Error('Please pass the array name.');
    }
    if (typeof params === 'undefined' || params.constructor !== Array) {
      throw new Error('Please pass an array of valid params obj.');
    }

    var _this = this;
    var errors = [];
    if (value && value.constructor === Array && value.length > 0 ) {
      value.forEach(function(obj) {
        errors = errors.concat(_this.alterErrorFields(name, _this.hasValidationErrors(params, obj)));
      });
    } else {
      errors.push(Errors.getParam(name));
    }
    return _.uniq(errors, JSON.stringify);
  },
  isValidFormat: function(type, value, opts) {
    var _this = this;
    var opts = opts? opts : {};

    if (typeof type === 'undefined') {
      throw new Error('Please pass a type.');
    }

    var isIn = (opts.in)? opts.in.indexOf(value) > -1 : true;

    switch (type) {
      case 'string':
        var isLength = (opts.minLength && opts.maxLength)? validator.isLength(value, opts.minLength, opts.maxLength) : true;
        return (typeof value == 'string' && value.trim() != '' && isLength && isIn);
      case 'int':
        return (value % 1 == 0);
      case 'number':
        var isPositive = (value >= 0 ? true : false);
        return  (validator.isFloat(value) || validator.isInt(value)) && isPositive;
      case 'date':
        return validator.isDate(value);
      default:
        return false;
    }
  },
  alterErrorFields: function(prefix, errors) {
    if (typeof prefix === 'undefined') {
      throw new Error('Please pass a prefix.');
    }
    if (typeof errors === 'undefined' || errors.constructor !== Array) {
      throw new Error('Please pass a valid error array.');
    }
    errors.forEach(function(error) {
      if (error && error.field) {
        error.field = prefix + '.' + error.field;
      }
    });
    return errors;
  },
  isWithinRangeValue: function(value, min, max) {
    if (parseInt(value) >= min && parseInt(value) <= max) {
      return true;
    } else {
      return false;
    }
  }
}
