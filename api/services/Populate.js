var TAG = '[Populate]';
module.exports = {
  addUser: function(data, cb) {
    var ACTION = '[addUser]';
    async.each(data, function(obj, cb) {
      User.create(obj, function(err, entry) {
        if(err) {
          sails.log.info('error', TAG + ACTION, err);
          return cb(Errors.raise('DB_ERROR'));
        }
        sails.log.info('info', TAG + ACTION + ' added entry', '(ID: ' + entry.id + ')');
        cb();
      });
    }, cb);
  },
  addElectricMeter: function(data, cb) {
    var ACTION = '[addElectricMeter]';
    async.each(data, function(obj, cb) {
      ElectricMeter.create(obj, function(err, entry) {
        if(err) {
          sails.log.info('error', TAG + ACTION, err);
          return cb(Errors.raise('DB_ERROR'));
        }
        sails.log.info('info', TAG + ACTION + ' added entry', '(ID: ' + entry.id + ')');
        cb();
      });
    }, cb);;
  },
  add: function(model, data, cb) {
    switch(model) {
      case 'user':
        this.addUser(data, cb);
      break;
      case 'electricmeter':
        this.addElectricMeter(data, cb);
      break;
      default:
        return cb(Errors.raise('NOT_FOUND'));
    }
  }
};
