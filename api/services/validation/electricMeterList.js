module.exports = [
  { name: 'from', type: 'date', required: true },
  { name: 'to', type: 'date', required: true },
  { name: 'groupBy', type: 'string', required: false, in: [ 'month', 'day', 'hour' ] },
];
