module.exports = function(req, res, next) {

  if (req.session.user) {
    return next();
  }

  if (!(req.headers.fbid && req.headers.accesstoken)) {
    return res.error(Errors.raise('UNAUTHORIZED'));
  }

  UserHelper.findOrCreate(req.headers.fbid, req.headers.accesstoken, function(err, user) {
    if (!err && user) {
      req.session.user = user;
    }
    next(err);
  });
};
