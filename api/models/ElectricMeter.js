module.exports = {
  attributes: {
    value: {
      type: 'float',
      required: true
    },
    user: {
      model: 'user',
      required: true
    }
  }
};