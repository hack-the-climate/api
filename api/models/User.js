module.exports = {
  attributes: {
    name: {
      type : 'string',
      required : true
    },
    fbId: {
      type : 'string',
      required : true
    },
    electricMeters: {
      collection: 'ElectricMeter',
      via: 'user'
    },
    aveRate: {
      type: 'float',
      defaultsTo: 10.00
    }
  }
};