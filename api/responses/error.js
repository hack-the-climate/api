module.exports = function (error) {
  var res = this.res;
  //Utility.logResponse('error', res.req.method + res.req.url, error);
  res.status(error.status);
  res.json( { error: error.error });
};